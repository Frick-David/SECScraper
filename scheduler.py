import schedule
import time
import datetime
from zoneinfo import ZoneInfo


def job_get_13F_master_idx():
    print(f"Working - {datetime.datetime.now()} ")
    # Get today in New York
    today_in_nyc = datetime.datetime.now(ZoneInfo("America/New_York"))
    date = today_in_nyc - datetime.timedelta(days=1)
    quarter = (date.month - 1) / 3 + 1

    # Process the master index for the previous day

    print("Done")


def job_get_current_13F_filings():
    # Check to make sure we are between 7:00 AM and 8:00PM in America/New_York
    this_hour = datetime.datetime.now(ZoneInfo("America/New_York")).hour
    if 7 <= this_hour <= 20:
        print(f"Working - {datetime.datetime.now()} ")
        # Stuff we want job to do.

        print("Done")
    else:
        print("Not doing work as not in 7AM - 8PM timeframe.")


# At 5:00 AM before the market opens
schedule.every().day.at("05:00:00").do(job_get_13F_master_idx)
schedule.every().hour.at(":45").do(job_get_current_13F_filings)
schedule.every().hour.at(":15").do(job_get_current_13F_filings)

while True:
    schedule.run_pending()
    time.sleep(1)

"""
This file will get the master.idx file and parse it to find all the filings of companies in a quarter in a year.
It can be tweaked to get different quarters and years.

An example URL we would want is: https://www.sec.gov/Archives/edgar/full-index/2023/QTR3/master.idx
which contains cik, company_name, form_type, date_filled, filename

an example line from this is `1000045|NICHOLAS FINANCIAL INC|10-Q|2023-08-16|edgar/data/1000045/0000950170-23-043019.txt`
which we can use to get the full filing by using edgar/data/1000045/0000950170-23-043019.txt removing the .txt and
removing the -s.
"""

# TODO: Might need to scan form 3 as well. See RC Ventures at https://www.sec.gov/edgar/browse/?CIK=1822844

import requests
from bs4 import BeautifulSoup
from datetime import date, timedelta, datetime
import re


class Parser:

    def __init__(self):
        self.BASE_URL = 'https://www.sec.gov'
        self.THIRTEEN_F_FORM_TYPES = ['13F-HR', '13F-HR/A']
        self.EXPECTED_COLUMN_SIZE = 5

    """
    Method to get the master.idx file that holds all the filing data for a quarter and year.
    
    Params:
        filing_year - the year we are trying to get all the filings for. ex 2014
        filing_quarter - the quarter we are trying to get the filings for. ex 3. Should be between 1-4 
    """

    def thirteen_f_filings(self, filing_year, filing_quarter):
        url = f"{self.BASE_URL}/Archives/edgar/full-index/{filing_year}/QTR{filing_quarter}/master.idx"

        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        r = requests.get(url, headers=headers)
        content = r.content.decode("utf-8")

        thirteen_fs = []

        for line in content.split('\n'):
            # The length of each line with | is 5. We only want those lines.
            split_line = line.split('|')
            if len(split_line) == self.EXPECTED_COLUMN_SIZE:
                # Check if it is the form we want ie 13-HR or 13-HR/A
                if split_line[2] in self.THIRTEEN_F_FORM_TYPES:
                    print(split_line, '\n')
                    # Get the filing URL which is the 5th element with the .txt and -s removed
                    full_submission_url = f"{self.BASE_URL}/Archives/{split_line[4]}"
                    dir_url = full_submission_url.replace('-', '').replace('.txt', '')

                    thirteen_fs.append({
                        'external_id': dir_url.split('/')[-2],
                        'company_name': split_line[1],
                        'form_type': split_line[2],
                        'cik': self.padded_cik(split_line[0]),
                        'date_filed': split_line[3],
                        'full_submission_url': full_submission_url,
                        'directory_url': dir_url
                    })

                    print(thirteen_fs)

                    # Get the xml filings
                    self.get_xml_filing_urls(dir_url)

        return thirteen_fs

    def get_xml_filing_urls(self, filing_url):
        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }
        r = requests.get(filing_url, headers=headers)

        # Parse the results using BeautifulSoup
        soup = BeautifulSoup(r.content, 'lxml')
        table = soup.find('table')

        # Loop throw the table rows and find the filer first
        for table_row in table.find_all('tr'):
            table_cells = table_row.find_all('td')
            if len(table_cells) > 0:
                link = table_cells[0]

                # If it is a xml file but is a primary_doc.xml file.
                if self.is_xml_file(link.text) and self.is_primary_doc_xml(link.text):
                    print(link.text)

                    # Get the href ot the link to make a followup request
                    href = link.find('a')['href']
                    filer_url = f"{self.BASE_URL}{href}"
                    print(filer_url)
                    self.extract_filer(filer_url)

        # Loop through again to find the holdings.
        # Loop throw the table rows and find the filer first
        for table_row in table.find_all('tr'):
            table_cells = table_row.find_all('td')
            if len(table_cells) > 0:
                link = table_cells[0]

                # If it is a xml file but not a primary_doc.xml file.
                if self.is_xml_file(link.text) and not self.is_primary_doc_xml(link.text):
                    print(link.text)

                    # Get the href ot the link to make a followup request
                    href = link.find('a')['href']
                    holdings_url = f"{self.BASE_URL}{href}"
                    print(holdings_url)
                    self.extract_holdings(holdings_url)

    def extract_holdings(self, holdings_url):
        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        r = requests.get(holdings_url, headers=headers)
        soup = BeautifulSoup(r.content, features="xml")
        records = soup.find_all("infoTable")
        for record in records:
            issuer_name = record.find("nameOfIssuer").text
            class_title = record.find("titleOfClass").text
            cusip = record.find('cusip').text
            value = record.find('value').text
            number_of_shares = record.find('shrsOrPrnAmt').find('sshPrnamt').text
            type = record.find('putCall')
            if type is None:
                type = 'Shares'
            else:
                type = type.text

            print(issuer_name, class_title, cusip, value, number_of_shares, type)

            # Insert the holding into the database.

    """
    Method to get the most recent filings 13F from the sec website through browsing.

    Params:
        filled_since - 
        per_page - 
        max_pages - 
    """

    def latest_thirteen_f_filings(self, filled_since=date.today() - timedelta(days=1), per_page=100, max_pages=10000):

        url = f"{self.BASE_URL}/cgi-bin/browse-edgar"

        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        query_params = {
            'action': "getcurrent",
            'count': per_page,
            'output': "atom",
            'type': "13F-HR"
        }

        title_regex = re.compile("\A13F\-HR(?:\/A)? - (.+?) \((\d{10})\)")

        results = []

        while (max_pages != 0):

            r = requests.get(url, params=query_params, headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')
            print(soup.prettify())

            entries = soup.find_all('entry')
            for entry in entries:
                date_filed = datetime.strptime(entry.find('updated').text.rsplit('-', 1)[0], "%Y-%m-%dT%H:%M:%S").date()
                print("Date Filled: " + str(date_filed))

                if date_filed < filled_since:
                    print(f"Date filed {date_filed} is less than filled since {filled_since}. Continuing.")
                    continue

                directory_url = entry.find('link')['href'].rsplit('/', 1)[0] + '/'
                print("Directory URL: " + directory_url)

                external_id = directory_url.split('/')[-2]
                print("external id: " + external_id)

                cik = self.padded_cik(directory_url.split("/")[-3])
                print("CIK:" + cik)

                form_type = entry.find("category", attrs={'label': 'form type'})["term"]
                print("Form Type:" + form_type)

                company_name = title_regex.search(entry.find('title').text)
                print("Company Name:" + company_name.group(1))

                results.append({
                    'external_id': external_id,
                    'company_name': company_name.group(1),
                    'form_type': form_type,
                    'cik': cik,
                    'date_filed': str(date_filed),
                    'directory_url': directory_url
                })

            # Increment the start of the page by per page.
            if "start" in query_params.keys():
                query_params['start'] += per_page
            else:
                query_params['start'] = per_page

            # Stop the loop if we are on the last page.
            if len(soup.find_all('entry')) < per_page:
                print("Found end of list - breaking")
                break

            max_pages -= 1

        # get the directory_url of each result and find the infotable.xml or InfoTable.xml to get the holdings.
        # It is the only other .xml file besides primary_doc.xml

        return results

    """
    Method to get the most recent filings Form 4 from the SEC website through browsing.

    Params:
        filled_since - 
        per_page - 
        max_pages - 
    """

    def latest_form_four_filings(self, filled_since=date.today() - timedelta(days=2), per_page=100, max_pages=10000):

        url = f"{self.BASE_URL}/cgi-bin/browse-edgar"

        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        query_params = {
            'action': "getcurrent",
            'count': per_page,
            'output': "atom",
            'type': ["4", "4/A"]
        }

        title_regex = re.compile("\A4(?:\/A)? - (.+?) \((\d{10})\)")

        results = []

        while (max_pages != 0):

            r = requests.get(url, params=query_params, headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')

            entries = soup.find_all('entry')
            for entry in entries:
                date_filed = datetime.strptime(entry.find('updated').text.rsplit('-', 1)[0], "%Y-%m-%dT%H:%M:%S").date()
                print("Date Filled: " + str(date_filed))

                # Skip entry if it is not recent
                if date_filed < filled_since:
                    print(f"Date filed {date_filed} is less than filled since {filled_since}. Continuing.")
                    continue

                # Skip entry if it is an extra form '4' that we do not want. We are only interested in Form 4 and 4/A
                form_type = entry.find("category", attrs={'label': 'form type'})["term"]
                print(form_type)
                if form_type != '4' and form_type != '4/A':
                    print(f"Found Form {form_type}. Skipping")
                    continue

                directory_url = entry.find('link')['href'].rsplit('/', 1)[0] + '/'
                print("Directory URL:" + directory_url)

                external_id = directory_url.split('/')[-2]
                print("external id: " + external_id)

                cik = self.padded_cik(directory_url.split("/")[-3])
                print("CIK:" + cik)

                company_name = title_regex.search(entry.find('title').text)
                print("Company Name:" + company_name.group(1))

                results.append({
                    'external_id': external_id,
                    'company_name': company_name.group(1),
                    'form_type': form_type,
                    'cik': cik,
                    'date_filed': str(date_filed),
                    'directory_url': directory_url
                })

            # Increment the start of the page by per page.
            if "start" in query_params.keys():
                query_params['start'] += per_page
            else:
                query_params['start'] = per_page

            # Stop the loop if we are on the last page.
            if len(soup.find_all('entry')) < per_page:
                print("Found end of list - breaking")
                break

            max_pages -= 1

        results = self.remove_duplicate_accession_filings(results)
        self.extract_form_4_transactions(results)

        return results

    """
       Method to get the most recent filings Form 4 from the SEC website through browsing.

       Params:
           filled_since - 
           per_page - 
           max_pages - 
    """

    def latest_sc_thirteen_d_filings(self, filled_since=date.today() - timedelta(days=2), per_page=100,
                                     max_pages=10000):

        url = f"{self.BASE_URL}/cgi-bin/browse-edgar"

        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        query_params = {
            'action': "getcurrent",
            'count': per_page,
            'output': "atom",
            'type': ["SC 13D/A"]
        }

        title_regex = re.compile("\ASC 13D(?:\/A)? - (.+?) \((\d{10})\)")

        results = []

        while (max_pages != 0):

            r = requests.get(url, params=query_params, headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')

            entries = soup.find_all('entry')
            for entry in entries:
                date_filed = datetime.strptime(entry.find('updated').text.rsplit('-', 1)[0], "%Y-%m-%dT%H:%M:%S").date()
                print("Date Filled: " + str(date_filed))

                # Skip entry if it is not recent
                if date_filed < filled_since:
                    print(f"Date filed {date_filed} is less than filled since {filled_since}. Continuing.")
                    continue

                # Skip entry if it is an extra form '4' that we do not want. We are only interested in Form 4 and 4/A
                form_type = entry.find("category", attrs={'label': 'form type'})["term"]
                print("Form type:" + form_type)

                directory_url = entry.find('link')['href'].rsplit('/', 1)[0] + '/'
                print("Directory URL:" + directory_url)

                external_id = directory_url.split('/')[-2]
                print("external id: " + external_id)

                cik = self.padded_cik(directory_url.split("/")[-3])
                print("CIK:" + cik)

                company_name = title_regex.search(entry.find('title').text)
                print("Company Name:" + company_name.group(1))

                results.append({
                    'external_id': external_id,
                    'company_name': company_name.group(1),
                    'form_type': form_type,
                    'cik': cik,
                    'date_filed': str(date_filed),
                    'directory_url': directory_url
                })

            # Increment the start of the page by per page.
            if "start" in query_params.keys():
                query_params['start'] += per_page
            else:
                query_params['start'] = per_page

            # Stop the loop if we are on the last page.
            if len(soup.find_all('entry')) < per_page:
                print("Found end of list - breaking")
                break

            max_pages -= 1

        self.all_have_non_primary_xml_docs(results)
        # None of the SC 13G/As have xml files. They all have .txt files.
        results = self.remove_duplicate_accession_filings(results)
        self.extract_sc_thirteen_d_transactions(results)

        return results

    """
    Detects whether the xml url provided is an xml file.
    """
    def is_xml_file(self, text):
        if text[-4:] == '.xml':
            return True
        return False

    """
    Detects whether the url provided is an .txt file.
    """
    def is_text_file(self, text):
        if text[-4:] == '.txt':
            return True
        return False

    """
    Detects whether the xml url provided is 'primary_doc.xml'
    """
    def is_primary_doc_xml(self, text):
        if text == 'primary_doc.xml':
            return True
        return False

    """
    Pads the CIK so it has leading zeros
    """
    def padded_cik(self, cik_to_pad):
        return str(cik_to_pad).strip().rjust(10, "0")

    def extract_filer(self, filer_url):
        pass

    def all_have_non_primary_xml_docs(self, results):
        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        for result in results:
            non_primary_xml_doc = False
            r = requests.get(result['directory_url'], headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')
            table = soup.find('table')

            # Loop throw the table rows and find the filer first
            for table_row in table.find_all('tr'):
                table_cells = table_row.find_all('td')
                if len(table_cells) > 0:
                    link = table_cells[0]

                    # If it is a xml file but is a primary_doc.xml file.
                    if self.is_xml_file(link.text):
                        non_primary_xml_doc = True
            if not non_primary_xml_doc:
                print(result)

    def extract_form_4_transactions(self, results):
        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        transaction_urls = []

        for result in results:
            r = requests.get(result['directory_url'], headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')
            table = soup.find('table')

            # Get the xml file on the page.
            for table_row in table.find_all('tr'):
                table_cells = table_row.find_all('td')
                if len(table_cells) > 0:
                    link = table_cells[0]

                    # If it is a xml file then retrieve it
                    if self.is_xml_file(link.text):
                        # TODO: clean up whole method to match format of extract_holdings.
                        # Get the href ot the link to make a followup request
                        href = link.find('a')['href']
                        transactions_url = f"{self.BASE_URL}{href}"
                        transaction_urls.append(transactions_url)

        data_to_check = []
        # Retrieve each of the transaction_url's pages to parse the xml.
        for transactions_url in transaction_urls:
            r = requests.get(transactions_url, headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')  # We now have the XML to parse

            print(soup.prettify())
            print(transactions_url)


            ownership_document = soup.find('ownershipdocument')
            issuer = ownership_document.find('issuer')
            issuer_cik = issuer.find('issuercik').text
            issuer_name = issuer.find('issuername').text
            ticker = issuer.find('issuertradingsymbol').text

            reporting_owner = ownership_document.find('reportingowner')
            insider_name = reporting_owner.find('reportingownerid').find('rptownername').text

            reporter_relationship = reporting_owner.find('reportingownerrelationship')
            title = ''

            # Construct the title
            if reporter_relationship.find('isdirector'):
                if reporter_relationship.find('isdirector').text == "1":
                    title += 'Dir '
            elif reporter_relationship.find('isofficer'):
                if reporter_relationship.find('isofficer').text == "1":
                    title += reporter_relationship.find('officertitle').text + ' '
            elif reporter_relationship.find('istenpercentowner'):
                if reporter_relationship.find('istenpercentowner').text == "1":
                    title += '10% '
            elif reporter_relationship.find('isother'):
                if reporter_relationship.find('isother').text == "1":
                    title += reporter_relationship.find('othertext').text + ' '
            else:
                title = None
            # TODO - Loop through nonderivative table and compare to deriavatives:
            #  SEE https://www.sec.gov/Archives/edgar/data/1177394/000112760223019867/xslF345X04/form4.xml
            nonderivative_table = ownership_document.find('nonderivativetable')

            # TODO: check deriative tabble as well.
            try:
                transactions_date = (nonderivative_table.find('nonderivativetransaction').find('transactiondate')
                                    .find('value')
                                    .text)
            except AttributeError as e:
                transactions_date = None

            try:
                sale_type = (nonderivative_table.find('transactionamounts')
                             .find('transactionacquireddisposedcode').find('value').text)
                if sale_type == 'D':
                    sale_type = 'S - Sale'
                elif sale_type == 'A':
                    sale_type = 'P - Purchase'
                elif sale_type == 'M':
                    sale_type = 'Sale - Sale+OE'
                else:
                    sale_type = None
            except AttributeError as e:
                sale_type = None


            try:
                price = (nonderivative_table.find('nonderivativetransaction').find('transactionamounts')
                     .find('transactionpricepershare').find('value').text)
            except AttributeError as e:
                price = 0
            try:
                quantity = (nonderivative_table.find('nonderivativetransaction').find('transactionamounts')
                        .find('transactionshares').find('value').text)
            except AttributeError as e:
                quantity = 0
            try:
                owns = (nonderivative_table.find('nonderivativetransaction')
                        .find('posttransactionamounts').find('sharesownedfollowingtransaction').find('value').text)
            except AttributeError as e:
                owns = 0
            percent_change = None
            if quantity is not None and price is not None:
                value = round(float(quantity) * float(price), 2)
            else:
                value = 0

            data_to_check.append([sale_type, price, quantity, owns, percent_change, value, transactions_url])

        for row in data_to_check:
            print(row)

        # TODO - Even some different accession numbers are the same report. Need to test all numbers against other records and see if the same.
        # TODO - Goldman sachs had two different accession numbers but it was the same person reporting the same stock holding with the same end value.


    def extract_sc_thirteen_d_transactions(self, results):
        headers = {
            'User-Agent': 'David Frick frick-david@protonmail.com',
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'www.sec.gov'
        }

        transaction_urls = []

        for result in results:
            r = requests.get(result['directory_url'], headers=headers)
            soup = BeautifulSoup(r.content, 'lxml')
            table = soup.find('table')

            # Get the xml file on the page.
            for table_row in table.find_all('tr'):
                table_cells = table_row.find_all('td')
                if len(table_cells) > 0:
                    link = table_cells[0]

                    # If it is a xml file then retrieve it
                    if self.is_text_file(link.text):
                        # TODO: clean up whole method to match format of extract_holdings.
                        # Get the href ot the link to make a followup request
                        href = link.find('a')['href']
                        transactions_url = f"{self.BASE_URL}{href}"
                        transaction_urls.append(transactions_url)

            data_to_check = []
            # Retrieve each of the transaction_url's pages to parse the xml.
            for transactions_url in transaction_urls:
                r = requests.get(transactions_url, headers=headers)
                soup = BeautifulSoup(r.content, 'lxml')  # We now have the XML to parse
                print(transactions_url)

                # TODO: figure out why there are multiple tables with very similiarly named companies.

        # TODO - Even some different accession numbers are the same report. Need to test all numbers against other records and see if the same.
        # TODO - Goldman sachs had two different accession numbers but it was the same person reporting the same stock holding with the same end value.



    """
    Detects where the Accession number is repeated, and removes duplicates. This ensures there is only one accounting of the form 4 information.
    Different CIKS can all be involved in filing the same form 4 so our data needs a way to tell when it is filed by more than one party.
    
    Params:
    results - the dictionary of data holding our directory_urls. The directory_urls needs to be split to get the accession 
              number and remove duplicates.
    """
    def remove_duplicate_accession_filings(self, results):
        accession_numbers = []
        filtered_results = []
        for result in results:
            split_url = result['directory_url'].split("/")
            # print("Split url: " + str(split_url))
            # print("Accession Number: " + split_url[-2])

            if split_url[-2] not in accession_numbers:
                # Add it as found
                accession_numbers.append(split_url[-2])
                # Add the result into filtered results
                filtered_results.append(result)
        return filtered_results


parser = Parser()
# parser.thirteen_f_filings(2022, 4)
# parser.latest_thirteen_f_filings()
parser.latest_sc_thirteen_d_filings()
# parser.latest_form_four_filings()

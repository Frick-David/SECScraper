from sqlalchemy import Integer, Column, ForeignKey
from sqlalchemy.orm import relationship, joinedload, subqueryload, Session

from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

conn_string = "sqlite:///sec.sqlite3"

engine = create_engine(conn_string)
Base.metadata.create_all(engine)    # Create all tables
Session = sessionmaker(bind=engine)
session = Session()


class ThirteenF(Base):
    __tablename__ = 'thirteenf'

    id = Column(Integer, primary_key=True)


    holdings = relationship('Holding')
    aggregate_holdings = relationship('AggregateHolding')

    @staticmethod
    def most_recent():
        session.query(ThirteenF).order_by(ThirteenF.amount.desc())



class Holding(Base):
    __tablename__ = 'holding'

    cik = Column(Integer, primary_key=True)
    filer_cik = Column(ForeignKey('filer.cik'))



class Filer(Base):
    __tablename__ = 'filer'

    cik = Column(Integer, primary_key=True)
    holdings = relationship('Holding')

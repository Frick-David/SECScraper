import requests
from bs4 import BeautifulSoup
import re

urls = [
    'https://www.sec.gov/Archives/edgar/data/1058239/000106299319004848/formsc13da.htm',
    'https://www.sec.gov/Archives/edgar/data/1822844/000092189523001480/sc13da712128005_06132023.htm',
    'https://www.sec.gov/Archives/edgar/data/807249/000080724923000139/0000807249-23-000139.txt',
    'https://www.sec.gov/Archives/edgar/data/1383395/000090266423005246/p23-2609sc13da.htm'
]


def get(url):
    headers = {
        'User-Agent': 'David Frick frick-david@protonmail.com',
        'Accept-Encoding': 'gzip, deflate',
        'Host': 'www.sec.gov'
    }

    print("-----------------------------------------------------------------------------------")
    print("URL: " + url)
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, 'html.parser')

    return r.text


def test_if_numbers(string_to_test):
    if string_to_test.isdigit():
        return True
    return False

def exclude_empty_children_and_breaks(children):
    parsed_children = []
    for child in children:
        if child.name != 'br' and not child.text.isspace():
            parsed_children.append(child)
    return parsed_children


def exclude_footnotes(text):
    text = text.strip()

    # Test for parenthesis ()
    left_parenthesis_location = text.find("(")
    right_parenthesis_location = text.find(')')

    # Test for brackets if that fails
    if left_parenthesis_location == -1 and right_parenthesis_location == -1:
        left_parenthesis_location = text.find("[")
        right_parenthesis_location = text.find(']')

    # Test if the letters between are numbers or letter
    is_numbers = test_if_numbers(text[left_parenthesis_location + 1: right_parenthesis_location])
    if left_parenthesis_location > 0 and right_parenthesis_location > 0 and is_numbers:
        text = text[0:left_parenthesis_location - 1] + text[right_parenthesis_location + 1:]
        print("Found a footnote")
    return text


def find_where_previous(text_content, tag_type, helper_text_to_find):
    soup = BeautifulSoup(text_content.replace(u'\xa0', '').encode('utf-8'), 'html.parser')
    holding_element = soup.find(tag_type, string=re.compile(helper_text_to_find))

    # Sometimes find with re.compile misses obvious hits that we need soup contains for
    if holding_element is None:
        edited_helper_text = helper_text_to_find.replace('(', '').replace(')', '').split(' ')[0]
        holding_element = soup.select_one(f'{tag_type}:-soup-contains({edited_helper_text})')

    has_not_been_found = True
    text_to_find = None

    # First - Try that element first - if there are any other characters besides the string we are searching for -- take that.
    if len(' '.join(exclude_footnotes(holding_element.text).split()).strip().replace(helper_text_to_find, '')) > 0:
        print("Found within base element")
        has_not_been_found = False
        text_to_find = holding_element.text.replace(helper_text_to_find, '')

    # Second - Try all siblings
    element = holding_element.find_previous_sibling()
    while element is not None and has_not_been_found:
        children = element.findChildren()
        children = exclude_empty_children_and_breaks(children)
        if children:
            if (not children[0].text.isspace() and not children[0].text.replace('_', '') == ''
                    and children[0].text != 'I.R.S. identification nos. of above persons (entities only)'):
                print("Children - Found within sibling element")
                text_to_find = children[0].text
                has_not_been_found = False
                break
        # else:
        if not element.text.isspace() and not element.text == '' and element.text != 'I.R.S. identification nos. of above persons (entities only)':
            print("No children - Found within sibling")
            text_to_find = element.text.replace('_', '').replace("\xa0", "")
            has_not_been_found = False
            break
        element = element.find_previous_sibling()

    # Third - Try siblings of next parent
    parent = holding_element.parent.find_previous_sibling()
    while parent and has_not_been_found:
        children = parent.findChildren()
        if children:
            if not children[0].text.isspace() and not children[0].text == '':
                print("Children - Found within sibling parent")
                text_to_find = children[0].text
                has_not_been_found = False
                break
        if has_not_been_found:
            for element in parent.children:
                if not element.text.isspace() and not element.text == '':
                    print("No Children - Found within sibling parent")
                    text_to_find = element.text
                    has_not_been_found = False
                    break
        parent = parent.find_previous_sibling()

    return text_to_find


def find_where_next(html_content, tag_type, helper_text_to_find):
    soup = BeautifulSoup(html_content, 'html.parser')
    holding_element = soup.find(tag_type, string=re.compile(helper_text_to_find))

    # Sometimes find with re.compile misses obvious hits that we need soup contains for
    if holding_element is None:
        edited_helper_text = helper_text_to_find.replace('(', '').replace(')', '').split(' ')[0]
        holding_element = soup.select_one(f'{tag_type}:-soup-contains({edited_helper_text})')

    has_not_been_found = True
    text_to_find = None

    # First - Try that element first - if there are any other characters besides the string we are searching for -- take that.
    if len(' '.join(exclude_footnotes(holding_element.text).split()).strip().replace(helper_text_to_find, '')) > 0:
        print("Text: " + ' '.join(exclude_footnotes(holding_element.text).split()).strip().replace(helper_text_to_find, ''))
        print("Found within base element")
        has_not_been_found = False
        text_to_find = holding_element.text.replace(helper_text_to_find, '')

    # Second - Try all siblings
    element = holding_element.find_next_sibling()
    while element is not None and has_not_been_found:
        children = element.findChildren()
        children = exclude_empty_children_and_breaks(children)
        if children:
            if (not children[0].text.isspace() and not children[0].text.replace('_', '') == ''
                    and children[0].text != 'I.R.S. identification nos. of above persons (entities only)'):
                print("Children - Found within sibling element")
                text_to_find = children[0].text
                has_not_been_found = False
                break
        # else:
        if not element.text.isspace() and not element.text == '' and element.text != 'I.R.S. identification nos. of above persons (entities only)':
            print("No children - Found within sibling")
            print(element)
            text_to_find = element.text.replace('_', '').replace("\xa0", "")
            has_not_been_found = False
            break
        element = element.find_next_sibling()

    # Third - Try siblings of next parent
    parent = holding_element.parent.find_next_sibling()
    while parent and has_not_been_found:
        children = parent.findChildren()
        if children:
            if not children[0].text.isspace() and not children[0].text == '':
                print("Children - Found within sibling parent")
                text_to_find = children[0].text
                has_not_been_found = False
                break
        if has_not_been_found:
            for element in parent.children:
                if not element.text.isspace() and not element.text == '':
                    print("No Children - Found within sibling parent")
                    text_to_find = element.text
                    has_not_been_found = False
                    break
        parent = parent.find_next_sibling()

    return text_to_find


text_content = get(urls[0])
name_reporting_person = find_where_next(text_content, 'p', "NAME OF REPORTING PERSON")
print("Reporting Name: " + str(name_reporting_person))

issuer_name = find_where_previous(text_content, 'p', "(Name of Issuer)")
print("Issuer Name: " + issuer_name)

cusip = find_where_previous(text_content, 'p', "(CUSIP Number)")
print("Cusip: " + cusip)

percentage = find_where_next(text_content, 'p', 'PERCENT OF CLASS REPRESENTED BY AMOUNT IN ROW')
print("Percentage: " + percentage)

date_of = find_where_previous(text_content, 'p', '(Date of Event Which Requires Filing of This Statement)')
print("Date of: " + date_of)


print("-----------------------------------------------------------------------------------")

text_content = get(urls[1])

name_reporting_person = find_where_next(text_content, 'td', 'NAME OF REPORTING PERSON')
print("REPORTING NAME: " + str(name_reporting_person))

name_of_issuer = find_where_previous(text_content, 'p', '(Name of Issuer)')
print("NAME: " + name_of_issuer)

cusip = find_where_previous(text_content, 'p', '(CUSIP Number)')
print("CUSIP: " + cusip)

percentage = find_where_next(text_content, 'td', 'PERCENT OF CLASS REPRESENTED BY AMOUNT IN ROW')
print("Percentage: " + percentage)



date_of = find_where_previous(text_content, 'p', '(Date of Event Which Requires Filing of This Statement)')
print("Date of: " + date_of)




print("-----------------------------------------------------------------------------------")


text_content = get(urls[2])

name_reporting_person = find_where_next(text_content, 'div', 'Names of reporting persons')
print("Reporting Name: " + str(name_reporting_person))

name_of_issuer = find_where_previous(text_content, 'div', '(Name of Issuer)')
print("Name of Issuer: " + name_of_issuer)

cusip = find_where_previous(text_content, 'div', '(CUSIP Number)')
print("CUSIP: " + cusip)

percentage = find_where_next(text_content, 'div', 'Percent of class represented by amount in row')
print("Percentage: " + percentage)


date_of = find_where_previous(text_content, 'div', '(Date of Event which Requires Filing of this Statement)')
print("Date of: " + date_of)


print("-----------------------------------------------------------------------------------")


text_content = get(urls[3])

name_reporting_person = find_where_next(text_content, 'p', 'NAME OF REPORTING PERSON')
print("Reporting Name: " + str(name_reporting_person))

name_of_issuer = find_where_previous(text_content, 'td', '(Name of Issuer)')
print("Name of Issuer: " + name_of_issuer)

cusip = find_where_previous(text_content, 'td', '(CUSIP Number)')
print("CUSIP: " + cusip)

percentage = find_where_next(text_content, 'p', 'PERCENT OF CLASS REPRESENTED BY AMOUNT IN ROW')
print("Percentage: " + percentage)


date_of = find_where_previous(text_content, 'td', '(Date of Event Which Requires Filing of This Statement)')
print("Date of: " + date_of)
